<?php

declare(strict_types=1);

namespace Tests\NLdoc\ElementStructure\Serialization;

use Exception;
use NLdoc\ElementStructure\Serialization\ElementStructureSerializer;
use NLdoc\ElementStructure\Types\Attribute\Asset;
use NLdoc\ElementStructure\Types\Attribute\DocumentElementAttributes;
use NLdoc\ElementStructure\Types\Attribute\EmptyAttributes;
use NLdoc\ElementStructure\Types\Attribute\HeadingElementAttributes;
use NLdoc\ElementStructure\Types\Attribute\ImageElementAttributes;
use NLdoc\ElementStructure\Types\Attribute\OrderedListElementAttributes;
use NLdoc\ElementStructure\Types\Attribute\OrderedListStyleType;
use NLdoc\ElementStructure\Types\Attribute\UnorderedListElementAttributes;
use NLdoc\ElementStructure\Types\Attribute\UnorderedListStyleType;
use NLdoc\ElementStructure\Types\Element\DocumentElement;
use NLdoc\ElementStructure\Types\Element\HeadingElement;
use NLdoc\ElementStructure\Types\Element\ImageElement;
use NLdoc\ElementStructure\Types\Element\ListMemberElement;
use NLdoc\ElementStructure\Types\Element\OrderedListElement;
use NLdoc\ElementStructure\Types\Element\QuotationElement;
use NLdoc\ElementStructure\Types\Element\TableElement;
use NLdoc\ElementStructure\Types\Element\TextElement;
use NLdoc\ElementStructure\Types\Element\UnorderedListElement;
use NLdoc\ElementStructure\Types\Validation\Checksum;
use NLdoc\ElementStructure\Types\Validation\HashAlgorithmType;
use NLdoc\ElementStructure\Types\Validation\ValidationMessage;
use NLdoc\ElementStructure\Types\Validation\ValidationMessageContext;
use NLdoc\ElementStructure\Types\Validation\ValidationMessageSeverity;
use NLdoc\ElementStructure\Types\Validation\ValidationMessageState;
use PHPUnit\Framework\TestCase;

class ElementStructureSerializerTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testSerializingDocumentOne(): void
    {
        $serializer = new ElementStructureSerializer();

        $element = $serializer->deserialize($this->loadFixture('one.json'));

        $this->assertEquals(
            new DocumentElement(
                "161dd93c-34e0-4a66-a814-4581c69ed0f1",
                [
                    new ImageElement(
                        "bcb45b2a-3b58-45f9-87d2-339b27e8078b",
                        new ImageElementAttributes("media/image1.jpeg", ""),
                        [
                            new ValidationMessage(
                                "87ca829e-21fd-4f01-9e98-df4c51894da5",
                                ValidationMessageSeverity::Warning,
                                "Attribute alt is required",
                                "attribute-alt-required-on-element",
                                new ValidationMessageContext(null),
                                new Checksum(
                                    HashAlgorithmType::SHA1,
                                    "11f6ad8ec52a2984abaafd7c3b516503785c2072"
                                ),
                                ValidationMessageState::Unresolved
                            )
                        ]
                    ),
                    new TextElement(
                        "ee08a01f-5371-449b-b822-1576328257ef",
                        "<p>Hier een stuk tektst</p>",
                        new EmptyAttributes(),
                        []
                    )
                ],
                new DocumentElementAttributes(title: 'HOND'),
                []
            ),
            $element
        );

        $this->assertEquals(
            json_decode($this->loadFixture('one.json')),
            json_decode($serializer->serialize($element))
        );
    }

    /**
     * @throws Exception
     */
    public function testSerializingDocumentTwo(): void
    {
        $serializer = new ElementStructureSerializer();

        $element = $serializer->deserialize($this->loadFixture('two.json'));

        $this->assertEquals(
            new DocumentElement(
                "65eb2eb0-60bb-48e9-9548-0e04709bf59b",
                [
                    new TextElement(
                        "580de759-e3b2-407c-9a4a-9c6c3c4de6a6",
                        "<p><a>Visit Free Test Data for more sample file sizes</a></p>"
                    ),
                    new HeadingElement(
                        "dce1d189-0380-400f-9b95-928d91a406a9",
                        "FREE TEST DATA",
                        new HeadingElementAttributes(level: 1),
                        [
                            new ValidationMessage(
                                "6666131b-a3f4-4986-8644-a6993bd3817a",
                                ValidationMessageSeverity::Error,
                                "This is an example validation message with severity 'error'",
                                "example-msg-error",
                                new ValidationMessageContext(null),
                                new Checksum(
                                    HashAlgorithmType::SHA512,
                                    "jdfijlsfjklfejklwesfjk"
                                ),
                                ValidationMessageState::Resolved
                            )
                        ]
                    ),
                    new HeadingElement(
                        "e46a07bc-3d14-4a60-a6f9-9edd9a61e509",
                        "Sample HTML File",
                        new HeadingElementAttributes(level: 2)
                    ),
                    new TextElement(
                        "98fb44bf-dc94-4e70-a9ac-d931837a78fe",
                        "<p>Lorem ipsum dolor sit amet.</p>",
                        new EmptyAttributes(),
                        [
                            new ValidationMessage(
                                "55bd5a45-711b-4450-839a-a8c3c046873a",
                                ValidationMessageSeverity::Info,
                                "This is an example validation message with severity 'info' and a selector",
                                "example-msg-info-with-selector",
                                new ValidationMessageContext("some-selector-1"),
                                new Checksum(
                                    HashAlgorithmType::SHA3_512,
                                    "xyz-1234"
                                ),
                                ValidationMessageState::Ignored
                            )
                        ]
                    ),
                    new UnorderedListElement(
                        "9ec0c4ab-bd1c-4ac4-a9a2-c26f18323ecf",
                        [
                            new ListMemberElement(
                                "59785268-1dcb-4e17-a14b-4a4e57874412",
                                "Suspendisse et odio tempor, venenatis leo at, feugiat mauris"
                            ),
                            new ListMemberElement(
                                "0284b1cc-72b3-4186-895f-044ce870eec4",
                                "lobortis et tortor nec, viverra pellentesque quam. ",
                                new EmptyAttributes(),
                                [
                                    new ValidationMessage(
                                        "ac6cdf0e-bcb3-4112-bd9f-6c431780afec",
                                        ValidationMessageSeverity::Error,
                                        "This is an example validation message with severity 'error' and a selector",
                                        "example-msg-error-with-selector",
                                        new ValidationMessageContext("some-selector-3"),
                                        new Checksum(
                                            HashAlgorithmType::BLAKE2b512,
                                            "0001112222333"
                                        ),
                                        ValidationMessageState::Unresolved
                                    )
                                ]
                            ),
                            new ListMemberElement(
                                "f42ce86e-c24e-4e54-acd1-06d02329bcb0",
                                "Curabitur purus felis, iaculis vitae nulla id, efficitur feugiat libero."
                            )
                        ],
                        new UnorderedListElementAttributes(
                            styleType: UnorderedListStyleType::Square
                        ),
                        [
                            new ValidationMessage(
                                "c5113c64-eabc-4605-8be2-15b2623ab01a",
                                ValidationMessageSeverity::Warning,
                                "This is an example validation message with severity 'warning'",
                                "example-msg-warning",
                                new ValidationMessageContext(null),
                                new Checksum(
                                    HashAlgorithmType::BLAKE2b512,
                                    "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
                                ),
                                ValidationMessageState::Unresolved
                            ),

                            new ValidationMessage(
                                "6b5b018c-dc0c-4993-8f3d-2d30bbc098e1",
                                ValidationMessageSeverity::Info,
                                "This is an example validation message with severity 'info'",
                                "example-msg-info",
                                new ValidationMessageContext(selector: null),
                                new Checksum(
                                    HashAlgorithmType::BLAKE2b512,
                                    "some-hash"
                                ),
                                ValidationMessageState::Ignored
                            ),
                        ]
                    ),
                    new OrderedListElement(
                        "9f67102f-d23e-40dd-9318-5f6b73e213c6",
                        [
                            new ListMemberElement(
                                "8c40f944-ae17-47ef-b2c3-580526afa009",
                                "Curabitur purus felis, iaculis vitae nulla id, efficitur feugiat libero."
                            ),
                            new ListMemberElement(
                                "37111e84-9ad4-46fb-ae9a-5b089233bfc1",
                                "Duis ornare ex sed nisl sollicitudin sollicitudin. In luctus gravida scelerisque"
                            ),
                        ],
                        new OrderedListElementAttributes(
                            styleType: OrderedListStyleType::RomanLower,
                            start: 3,
                            reversed: true
                        ),
                        [
                            new ValidationMessage(
                                "d0768fd3-e1d7-458c-9e18-6b5ad4800162",
                                ValidationMessageSeverity::Warning,
                                "This is an example validation message with severity 'warning'",
                                "example-msg-warning",
                                new ValidationMessageContext(null),
                                new Checksum(
                                    HashAlgorithmType::BLAKE2b512,
                                    "xyz"
                                ),
                                ValidationMessageState::Unresolved
                            )
                        ]
                    ),
                    new HeadingElement(
                        "fa2de87d-9d8f-4058-9a92-fb86373efd70",
                        "Foto van de koe",
                        new HeadingElementAttributes(level: 2)
                    ),
                    new ImageElement(
                        "9f7b4c7b-fd93-4df1-bf66-5e3e86ac3161",
                        new ImageElementAttributes("image.jpeg", ""),
                        [
                            new ValidationMessage(
                                "00000001-2003-4004-8005-040000012000",
                                ValidationMessageSeverity::Info,
                                "This is an example validation message with severity 'info'",
                                "example-msg-info",
                                new ValidationMessageContext(null),
                                new Checksum(
                                    HashAlgorithmType::SHA1,
                                    "11111"
                                ),
                                ValidationMessageState::Unresolved
                            )
                        ]
                    ),
                    new TableElement(
                        "04a9b7e8-dcc7-47e8-b82a-dc70d4d1720e",
                        "<table border=\"1\" cellpadding=\"1\" cellspacing=\"1\"></table>"
                    ),
                    new QuotationElement(
                        "01c359da-7010-428a-a4aa-7f9346fea708",
                        "<p>Dit is een citaat!</p>",
                        new EmptyAttributes(),
                        [
                            new ValidationMessage(
                                "12345678-0000-4000-8000-000000000000",
                                ValidationMessageSeverity::Error,
                                "This is an example validation message with severity 'error'",
                                "example-msg-error",
                                new ValidationMessageContext(),
                                new Checksum(
                                    HashAlgorithmType::BLAKE2b512,
                                    "1234iojwefjwekl"
                                ),
                                ValidationMessageState::Unresolved
                            )
                        ]
                    )
                ],
                new DocumentElementAttributes(
                    title: "Sample Document",
                    subtitle: "Some subtitle",
                    titlePrefix: "PREFIX",
                    language: "nl",
                    direction: "rtl",
                    category: "examples",
                    date: "2021-09-01",
                    keywords: ["never", "gonna", "give", "you", "up"],
                    authors: ["NLDOC"],
                    assets: [
                        new Asset('some-file', '/location/on/s3'),
                        new Asset('some-other-file', '/other/location/on/s3'),
                    ],
                ),
                [
                    new ValidationMessage(
                        "00000000-0000-4000-8000-000000000000",
                        ValidationMessageSeverity::Warning,
                        "This is an example validation message with severity 'warning' and a selector",
                        "example-msg-warning-with-selector",
                        new ValidationMessageContext("some-selector-1"),
                        new Checksum(
                            HashAlgorithmType::BLAKE2b512,
                            "1234iojwefjwekl"
                        ),
                        ValidationMessageState::Unresolved
                    )
                ]
            ),
            $element
        );

        $this->assertEquals(
            json_decode($this->loadFixture('two.json')),
            json_decode($serializer->serialize($element))
        );
    }

    /**
     * @throws Exception
     */
    private function loadFixture(string $filename): string
    {
        $contents = file_get_contents(__DIR__ . '/fixtures/' . $filename);

        if (!$contents) {
            throw new Exception('Could not load fixture ' . $filename);
        }

        return $contents;
    }
}
