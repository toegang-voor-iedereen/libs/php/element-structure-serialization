<?php

declare(strict_types=1);

namespace NLdoc\ElementStructure\Serialization;

use NLdoc\ElementStructure\Types\Element\DocumentElement;
use NLdoc\ElementStructure\Types\Element\HeadingElement;
use NLdoc\ElementStructure\Types\Element\ImageElement;
use NLdoc\ElementStructure\Types\Element\ListMemberElement;
use NLdoc\ElementStructure\Types\Element\OrderedListElement;
use NLdoc\ElementStructure\Types\Element\QuotationElement;
use NLdoc\ElementStructure\Types\Element\RootElementInterface;
use NLdoc\ElementStructure\Types\Element\TableElement;
use NLdoc\ElementStructure\Types\Element\TextElement;
use NLdoc\ElementStructure\Types\Element\UnorderedListElement;
use Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping;
use Symfony\Component\Serializer\Mapping\ClassDiscriminatorResolverInterface;

/**
 * @method array getSupportedTypes(?string $format)
 */
class ElementStructureClassDiscriminatorResolver implements ClassDiscriminatorResolverInterface
{
    /**
     * @param string $class
     * @return bool
     */
    private function handles(string $class): bool
    {
        return RootElementInterface::class === $class || in_array($class, array_values($this->getMap()), true);
    }

    /**
     * @return array<string, class-string>
     * @psalm-return array{
     *     'list-member': ListMemberElement::class,
     *     'list-ordered': OrderedListElement::class,
     *     'list-unordered': UnorderedListElement::class,
     *     document: DocumentElement::class,
     *     heading: HeadingElement::class,
     *     image: ImageElement::class,
     *     quotation: QuotationElement::class,
     *     table: TableElement::class,
     *     text: TextElement::class
     * }
     */
    private function getMap(): array
    {
        return [
            'document' => DocumentElement::class,
            'image' => ImageElement::class,
            'text' => TextElement::class,
            'list-unordered' => UnorderedListElement::class,
            'list-ordered' => OrderedListElement::class,
            'quotation' => QuotationElement::class,
            'heading' => HeadingElement::class,
            'table' => TableElement::class,
            'list-member' => ListMemberElement::class
        ];
    }

    public function getMappingForClass(string $class): ?ClassDiscriminatorMapping
    {
        if (!$this->handles($class)) {
            return null;
        }

        return new ClassDiscriminatorMapping('type', $this->getMap());
    }

    public function getMappingForMappedObject(object|string $object): ?ClassDiscriminatorMapping
    {
        return $this->getMappingForClass(is_string($object) ? $object : get_class($object));
    }

    public function getTypeForMappedObject(object|string $object): ?string
    {
        return $this->getMappingForMappedObject($object)?->getMappedObjectType($object);
    }
}
